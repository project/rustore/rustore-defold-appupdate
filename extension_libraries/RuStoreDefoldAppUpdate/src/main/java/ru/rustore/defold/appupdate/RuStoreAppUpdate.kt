package ru.rustore.defold.appupdate

import android.app.Activity
import android.net.Uri
import com.google.gson.GsonBuilder
import ru.rustore.defold.core.JsonBuilder
import ru.rustore.defold.core.RuStoreCore
import ru.rustore.defold.core.UriTypeAdapter
import ru.rustore.sdk.appupdate.listener.InstallStateUpdateListener
import ru.rustore.sdk.appupdate.manager.RuStoreAppUpdateManager
import ru.rustore.sdk.appupdate.manager.factory.RuStoreAppUpdateManagerFactory
import ru.rustore.sdk.appupdate.model.AppUpdateInfo
import ru.rustore.sdk.appupdate.model.AppUpdateOptions
import ru.rustore.sdk.appupdate.model.AppUpdateType
import ru.rustore.sdk.appupdate.model.InstallState

object RuStoreAppUpdate: InstallStateUpdateListener {
    private const val CHANNEL_GET_APP_UPDATE_INFO_SUCCESS = "rustore_get_app_update_info_success"
    private const val CHANNEL_GET_APP_UPDATE_INFO_FAILURE = "rustore_get_app_update_info_failure"
    private const val CHANNEL_START_UPDATE_FLOW_SUCCESS = "rustore_start_update_flow_success"
    private const val CHANNEL_START_UPDATE_FLOW_FAILURE = "rustore_start_update_flow_failure"
    private const val CHANNEL_COMPLETE_UPDATE_FAILURE = "rustore_complete_update_failure"
    private const val CHANNEL_ON_STATE_UPDATED = "rustore_on_state_updated"

    private lateinit var updateManager: RuStoreAppUpdateManager
    private var appUpdateInfo: AppUpdateInfo? = null
    private var isInitialized: Boolean = false
    private val gson = GsonBuilder()
        .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
        .create()

    fun init(activity: Activity) {
        if (isInitialized) return
        updateManager = RuStoreAppUpdateManagerFactory.create(
            context = activity.application,
            internalConfig = mapOf("type" to "defold")
        )

        isInitialized = true
    }

    fun registerListener(): Boolean {
        if (!isInitialized) return false
        updateManager.registerListener(this)

        return true
    }

    fun unregisterListener(): Boolean {
        if (!isInitialized) return false
        updateManager.unregisterListener(this)

        return true
    }

    fun getAppUpdateInfo() {
        updateManager.getAppUpdateInfo().addOnSuccessListener { result ->
            appUpdateInfo = result
            RuStoreCore.emitSignal(CHANNEL_GET_APP_UPDATE_INFO_SUCCESS, gson.toJson(appUpdateInfo))
        }.addOnFailureListener { throwable ->
            RuStoreCore.emitSignal(CHANNEL_GET_APP_UPDATE_INFO_FAILURE, JsonBuilder.toJson(throwable))
        }
    }

    fun checkIsImmediateUpdateAllowed(): Boolean {
        appUpdateInfo?.run {
            return isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
        }

        return false;
    }

    fun startUpdateFlowImmediate() {
        startUpdateFlow(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.IMMEDIATE).build())
    }

    fun startUpdateFlowSilent() {
        startUpdateFlow(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.SILENT).build())
    }

    fun startUpdateFlowDelayed() {
        startUpdateFlow(AppUpdateOptions.Builder().build())
    }

    private fun startUpdateFlow(appUpdateOptions: AppUpdateOptions) {
        appUpdateInfo?.let {
            updateManager.startUpdateFlow(it, appUpdateOptions)
                .addOnSuccessListener { flowResult ->
                    RuStoreCore.emitSignal(
                        CHANNEL_START_UPDATE_FLOW_SUCCESS,
                        "{\"flowResult\": ${flowResult}}"
                    )
                }
                .addOnFailureListener { throwable ->
                    RuStoreCore.emitSignal(CHANNEL_START_UPDATE_FLOW_FAILURE, JsonBuilder.toJson(throwable))
                }
        }
    }

    fun completeUpdateSilent() {
        completeUpdate(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.SILENT).build())
    }

    fun completeUpdateFlexible() {
        completeUpdate(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.FLEXIBLE).build())
    }

    private fun completeUpdate(appUpdateOptions: AppUpdateOptions) {
        updateManager.completeUpdate(appUpdateOptions).addOnFailureListener { throwable ->
            RuStoreCore.emitSignal(CHANNEL_COMPLETE_UPDATE_FAILURE, JsonBuilder.toJson(throwable))
        }
    }

    override fun onStateUpdated(state: InstallState) {
        RuStoreCore.emitSignal(CHANNEL_ON_STATE_UPDATED, gson.toJson(state))
    }
}
