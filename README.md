## RuStore Defold плагин для обновления приложения

### [🔗 Документация разработчика][10]

Плагин “RuStoreDefoldAppUpdate” позволяет пользователю оставить оценку и отзыв о вашем приложении в RuStore, не выходя из приложения.

Репозиторий содержит плагины “RuStoreDefoldAppUpdate” и “RuStoreDefoldCore”, а также демонстрационное приложение с примерами использования и настроек. Поддерживаются версии Defold 1.6.2+.


### Сборка примера приложения

Вы можете ознакомиться с демонстрационным приложением содержащим представление работы всех методов sdk:
- [README](appupdate_example/README.md)
- [appupdate_example](https://gitflic.ru/project/rustore/rustore-defold-appupdate/file?file=appupdate_example)


### Установка плагина в свой проект

1. Скопируйте папки _“appupdate_example / extension_rustore_appupdate”_ и _“appupdate_example / extension_rustore_core”_ в корень вашего проекта.


### Пересборка плагина

Если вам необходимо изменить код библиотек плагинов, вы можете внести изменения и пересобрать подключаемые .jar файлы.

1. Откройте в вашей IDE проект Android из папки _“extension_libraries”_.

2. Выполните сборку проекта командой gradle assemble.

При успешном выполнении сборки в папках _“appupdate_example / extension_rustore_appupdate / lib / android”_ и _“appupdate_example / extension_rustore_core / lib / android”_ будут обновлены файлы:
- RuStoreDefoldAppUpdate.jar
- RuStoreDefoldCore.jar


### История изменений

[CHANGELOG](CHANGELOG.md)


### Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](MIT-LICENSE.txt).


### Техническая поддержка

Дополнительная помощь и инструкции доступны на странице [rustore.ru/help/](https://www.rustore.ru/help/) и по электронной почте [support@rustore.ru](mailto:support@rustore.ru).

[10]: https://www.rustore.ru/help/sdk/updates/defold/6-0-0
