require("main.gui.menu_consts")
require("main.gui.progress_bar_consts")

local THIS_RECEIVER = "background#main"
local LOG_TAG = "ssss"

function init(self)
	msg.post("@render:", "use_fixed_fit_projection", { near = -1, far = 1 })

	rustorecore.connect("rustore_get_app_update_info_success", _on_get_app_update_info_success)
	rustorecore.connect("rustore_get_app_update_info_failure", _on_get_app_update_info_failure)
	rustorecore.connect("rustore_start_update_flow_success", _on_start_update_flow_success)
	rustorecore.connect("rustore_start_update_flow_failure", _on_start_update_flow_failure)
	rustorecore.connect("rustore_complete_update_failure", _on_complete_update_failure)
	rustorecore.connect("rustore_on_state_updated", _on_state_updated)
	rustoreappupdate.init()
	rustoreappupdate.register_listener()

	-- Menu
	msg.post(MENU_RECEIVER, MENU_SET_RECEIVER_MESSAGE_ID, { receiver = THIS_RECEIVER })

	-- Progress bar
	msg.post(PROGRESS_BAR_RECEIVER, PROGRESS_BAR_SET_PERCENT_MESSAGE_ID, { percent = 0 })
end

function on_message(self, message_id, message, sender)
	if message_id == hash(MENU_BUTTON_PRESSED) then
		if message.button == MENU_GET_APP_UPDATE_INFO then
			rustoreappupdate.get_appupdateinfo()
		end

		if message.button == MENU_IS_IMMEDIATE_UPDATE_ALLOWED then
			local result = rustoreappupdate.check_is_immediate_update_allowed()
			local message = "Immediate update: "..tostring(result)
			rustorecore.show_toast(message)
		end

		if message.button == MENU_START_UPDATE_FLOW_IMMEDIATE then
			rustoreappupdate.start_update_flow_immediate()
		end

		if message.button == MENU_START_UPDATE_FLOW_SILENT then
			rustoreappupdate.start_update_flow_silent()
		end

		if message.button == MENU_START_UPDATE_FLOW_DELAYED then
			rustoreappupdate.start_update_flow_delayed()
		end

		if message.button == MENU_COMPLETE_UPDATE_SILENT then
			rustoreappupdate.complete_update_silent()
		end

		if message.button == MENU_COMPLETE_UPDATE_FLEXIBLE then
			rustoreappupdate.complete_update_flexible()
		end
	end
end

function _on_get_app_update_info_success(self, channel, value)
	-- json fields:
	-- appId - number
	-- appName - string
	-- availableVersionCode - number
	-- fileSize - number
	-- forceUpdateAvailable - boolean
	-- iconUrl - string
	-- installStatus - number
	-- isUsed - boolean
	-- packageName - string
	-- updateAvailability - number
	-- updatePriority - number
	-- updatedAt - string

	rustorecore.log_warning(LOG_TAG, value)

	local data = json.decode(value)

	if data.updateAvailability == 0 then
		rustorecore.show_toast("UNKNOWN")
	elseif data.updateAvailability == 1 then
		rustorecore.show_toast("UPDATE_NOT_AVAILABLE")
	elseif data.updateAvailability == 2 then
		rustorecore.show_toast("UPDATE_AVAILABLE")
	elseif data.updateAvailability == 3 then
		rustorecore.show_toast("DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS")
	else
		rustorecore.show_toast(tostring(data.updateAvailability))
	end
end

function _on_get_app_update_info_failure(self, channel, value)
	-- json fields:
	-- detailMessage - string

	rustorecore.log_warning(LOG_TAG, value)

	local data = json.decode(value)
	rustorecore.show_toast(data.detailMessage)
end

function _on_start_update_flow_success(self, channel, value)
	-- json fields:
	-- flowResult - number

	rustorecore.log_warning(LOG_TAG, value)

	local data = json.decode(value)

	if data.flowResult == -1 then
		rustorecore.show_toast("RESULT_OK")
	elseif data.flowResult == 0 then
		rustorecore.show_toast("RESULT_CANCELED")
	elseif data.flowResult == 2 then
		rustorecore.show_toast("ACTIVITY_NOT_FOUND")
	else
		rustorecore.show_toast(tostring(data.flowResult))
	end
end

function _on_start_update_flow_failure(self, channel, value)
	-- json fields:
	-- code - number?
	-- detailMessage - string

	rustorecore.log_warning(LOG_TAG, value)

	local data = json.decode(value)
	local message = ""
	if not code == nil then
		message = tostring(data.code).." - "
	end
	
	message = message..data.detailMessage
	
	rustorecore.show_toast(message)
end

function _on_complete_update_failure(self, channel, value)
	-- json fields:
	-- detailMessage - string

	rustorecore.log_warning(LOG_TAG, value)

	local data = json.decode(value)
	rustorecore.show_toast(data.detailMessage)
end

function _on_state_updated(self, channel, value)
	-- json fields:
	-- bytesDownloaded - number
	-- totalBytesToDownload - number
	-- packageName - string
	-- installStatus - number
	-- installErrorCode - number

	rustorecore.log_warning(LOG_TAG, value)

	local data = json.decode(value)

	local percentDownloaded = 0
	if data.totalBytesToDownload > 0 then
		percentDownloaded = data.bytesDownloaded / data.totalBytesToDownload * 100
	end
	
	if data.installStatus == 1 then
		percentDownloaded = 100
	end

	percentDownloaded = math.floor(percentDownloaded * 100 + 0.5) / 100

	msg.post(PROGRESS_BAR_RECEIVER, PROGRESS_BAR_SET_PERCENT_MESSAGE_ID, { percent = percentDownloaded })
end
