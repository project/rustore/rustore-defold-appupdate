## История изменений

### Release 6.0.0
- Версия SDK appUpdate 6.0.0.


### Release 3.0.0
- Версия SDK appUpdate 3.0.0.
- Добавлено поле `simpleName` в JSON ошибок.


### Release 2.0.0
- Версия SDK appUpdate 2.0.0.


### Release 1.0.1
- Версия SDK appUpdate 1.0.1.
